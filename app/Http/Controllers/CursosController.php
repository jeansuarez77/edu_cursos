<?php

namespace App\Http\Controllers;

use App\Http\Requests\CursoRequest;

use App\Models\Curso;
use App\Models\Aula;
use App\Models\Programa;
use App\Models\Estudiante;
use App\Models\Ubicacione;

use Mappweb\Mappweb\Helpers\Util;
use Illuminate\Http\Request;

use DB;

class CursosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $buscador_curso  = trim(request("buscador_curso") ? request("buscador_curso"): "");
        $tipo_oferta  = trim(request("tipo_oferta") >= 0 ? request("tipo_oferta"): "");

        $query =  Curso::select('cursos.*','p.nombre as programa','u.nombre as sede')
        ->join('programas as p', 'p.id', '=', 'cursos.programa_id')
        ->join('ubicaciones as u', 'u.id', '=', 'cursos.sede_id');

        if(!empty($buscador_curso )){

            $query->where(function($query) use ($buscador_curso )
            {		 	
                $query->where('nombre_curso', 'like', "%".$buscador_curso."%");  
                $query->orWhere('modalidad', 'like', "%".$buscador_curso."%");  
                $query->orWhere('estado', 'like', "%".$buscador_curso."%"); 
                //$query->orWhere('programa', 'like', "%".$buscador_curso."%"); 
            });
        }
        if(!empty($tipo_oferta ) || $tipo_oferta >= 0){
            $query->where('tipo_oferta',$tipo_oferta); 
        }

        $data['buscador_curso'] = $buscador_curso ;
        $data['tipo_oferta'] = $tipo_oferta ;
        $data['cursos'] = $query->paginate()->appends(request()->query());

        
        return view("cursos.index",$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['object'] = new Curso;
        $data['programas'] = Programa::all();
        $data['sedes'] = Ubicacione::all();
        $data['vigencia_certificado'] = Curso::$VIGENCIA_CERTIFICADO;

        return view('cursos.modal-registro', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CursoRequest $request)
    {

        $programa = Programa::find(request('programa_id'));

        $sede = Ubicacione::find(request('sede_id'));

        $consecutivo = Curso::max('consecutivo');

        $objets = false;

        if($programa->count()){
            $data_input = array(
                'programa_id'   => $programa->id,
                'nombre_curso'  => $programa->nombre,
                'fecha_inicio'  => request('fecha_inicio'),
                'fecha_fin'     => request('fecha_fin'),
                'modalidad'     => request('modalidad'),
                'duracion'      => request('duracion'),
                'sede_id'       => request('sede_id'),
                'direccion'     => request('direccion'),
                'tipo_jornada'  => request('tipo_jornada'),
                'tipo_oferta'  => request('tipo_oferta'),
                'vigencia_certificado'  => request('vigencia_certificado'),
                'prefijo'     => $sede->prefijo,
                'consecutivo'  => $consecutivo + 1,
            );

            $objets = Util::updateOrCreate(Curso::class, $data_input);

        }
        
        $data['success'] =(boolean) $objets;
        $data['reload_page'] = true;    
        
        Util::addToastToData($data);

        return response()->crud($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function show($curso)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function edit($curso)
    {
        $data['object'] = Curso::find($curso);
        $data['programas'] = Programa::all();

        $data['vigencia_certificado'] = Curso::$VIGENCIA_CERTIFICADO;

        $data['sedes'] = Ubicacione::all();
        
        return view('cursos.modal-registro', $data);

    }

    public function edit_estudiantes($curso)
    {
        $data_curso = Curso::find($curso);
        $data['object'] = Aula::select('aulas.*','e.tipo_documento','e.documento_n','e.nombre','e.segundo_nombre','e.apellido','e.apellido2','e.email','e.celular')
        ->join('estudiantes as e', 'e.id', '=', 'aulas.estudiante_id')
        ->where('curso_id',$curso)
        ->where('es_aprobado',0)
        ->get();

        $data['programas'] = Programa::all();
        $data['curso_id'] = $curso;
        $data['has_abierto'] = $data_curso->estado=="Abierto"?true:false;

        return view('cursos.modal-estudiantes', $data);

    }

    public function aprobar_estidiantes(Request $request){

        try {
            DB::beginTransaction();

            $data = json_decode(request("data"));
            $data['success'] = false;

            foreach ($data as $value) {
                Aula::where('id',$value)
                ->update(['es_aprobado' => 1, 'fecha_finalizado' => date("Y-m-d")]);

                $data['success'] = true;
                
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            $data['success'] = false;
        }
        
        
        return response()->crud($data);
    }

    public function agregar_estidiantes(){
        //$objets = Util::updateOrCreate(Curso::class, $request, $id);
        $existe= Aula::select('estudiante_id')
        ->where('estudiante_id',request('estudiante_id'))
        ->where('curso_id',request('curso_id'))
        ->where('es_aprobado',0)
        ->first();

        $data['object'] = null;
        $data['success'] = false;
        $data['existe'] = false;
        
        if(empty($existe->estudiante_id)){
            $aula = new Aula;
            $aula->estudiante_id = request('estudiante_id');
            $aula->curso_id = request('curso_id');
            $aula->es_aprobado = 0;

            if($aula->save()){
                $estudiante = Estudiante::find(request('estudiante_id'));
                $estudiante->id_aula = $aula->id;
                $data['object'] = $estudiante;
                $data['success'] =true;
            }
        }else{

            $data['existe'] = true;
        }
        
        return response()->crud($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function update(CursoRequest $request,$id)
    {
        
        $objets = Util::updateOrCreate(Curso::class, $request, $id);

        $programa = Programa::find(request('programa_id'));


        if($programa->count()){
            $data_input = array(
                'nombre_curso'  => $programa->nombre,
            );

            Util::updateOrCreate(Curso::class, $data_input, $id);

        }
        
        $data['success'] =(boolean) $objets;
        $data['reload_page'] = true;    
        
        Util::addToastToData($data);

        return response()->crud($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function destroy(Curso $curso)
    {
        //
    }

    public function get_sugerencia()
    {
        $search = request("term");    
        $limit = 30;
        $data_sugerencia =  DB::table("cursos as c")->select(["c.nombre_curso as label","c.id as id"])->
        where(function($query) use ($search)
        {		 	
            $query->where('c.nombre_curso', 'like', "%".$search."%");      
        })
        ->limit(30)->get()->toArray();
        

        $data =  DB::table("cursos as c")->select(["c.modalidad as label","c.id as id"])->
        where(function($query) use ($search)
        {		 	
            $query->where('c.modalidad', 'like', "%".$search."%");  
        })
        ->limit(30)->get()->toArray();

        foreach ($data as $val) {
            $data_sugerencia[] = $val;
        }

        $data =  DB::table("cursos as c")->select(["c.tipo_jornada as label","c.id as id"])->
        where(function($query) use ($search)
        {		 	
            $query->where('c.tipo_jornada', 'like', "%".$search."%");  
        })
        ->limit(30)->get()->toArray();

        foreach ($data as $val) {
            $data_sugerencia[] = $val;
        }

        return response()->json($data_sugerencia,200);

    }
}
