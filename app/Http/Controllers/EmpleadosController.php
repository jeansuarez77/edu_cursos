<?php

namespace App\Http\Controllers;
use App\Http\Requests\EmpleadoRequest;

use Illuminate\Http\Request;
use Mappweb\Mappweb\Helpers\Util;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Rules\Password;
use Illuminate\Support\Facades\Auth;


use App\Models\User;

class EmpleadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["empleados"] = User::paginate(50);
        return view('empleados.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['object'] = new User;
        return view('empleados.modal-registro', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmpleadoRequest $request)
    {
        $data_input = array(
            'name'  => request('name'),
            'email'     => request('email'),
            'password'     => Hash::make(request('password')),
        );
        $objets = Util::updateOrCreate(User::class, $data_input);

        $data['success'] = (boolean) $objets;

        Util::addToastToData($data);

        return response()->crud($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['object'] = User::find($id);
        
        return view('empleados.modal-registro', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data_input = array(
            'name'  => request('name'),
            'email'     => request('email'),
        );
        if(empty(request('password'))){
            Validator::make($data_input, [
                'name' => ['required', 'string', 'max:255','unique:users,name,'.$id],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$id],
            ])->validate();

        }else{
            $data_input["password"] =  request('password');
            $data_input["password_confirmation"] =  request('password_confirmation');
            Validator::make($data_input, [
                'name' => ['required', 'string', 'max:255','unique:users,name,'.$id],
                'email' => ['required', 'string', 'email', 'max:255','unique:users,email,'.$id],
                'password' => [new Password, 'confirmed'],
            ])->validate();

            $data_input["password"] =  Hash::make(request('password'));
            $data['reload_page'] = true;

        }

        $objets = Util::updateOrCreate(User::class, $data_input,$id);

        $data['success'] = (boolean) $objets;

        Util::addToastToData($data);

        return response()->crud($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['success'] = false;

        if($id != Auth::id() && User::count()>1){
            $user = User::find($id);
            $data['success'] = $user->delete();
        }
        
        return response()->crud($data);

    }

    public function profile(){
        $data["object"] = User::find(Auth::id());

        return view('profile.show', $data);

    }
}
