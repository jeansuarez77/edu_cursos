<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Estudiante;
use App\Models\User;
use App\Http\Requests\EstudianteRequest;
use DB;
use Mappweb\Mappweb\Helpers\Util;


class EstudiantesController extends Controller
{

    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $busqueda = trim(request("buscar") ? request("buscar"): "");
        
        $query =  Estudiante::select('estudiantes.*',DB::raw('CAST(estudiantes.codigo AS CHAR) AS codigo'),);
        if(!empty($busqueda)){
            $query->where(function($query) use ($busqueda)
            {		 	
                $query->where('documento_n', 'like', "%".$busqueda."%");  
                $query->orWhere('email', 'like', "%".$busqueda."%");  
                $query->orWhere(DB::raw("CONCAT(nombre, ' ', segundo_nombre, ' ', apellido, ' ', apellido2)"), 'LIKE', "%".$busqueda."%");  
            });
        }
        $data['busqueda'] = $busqueda;
        $data['estudiantes'] = $query->paginate()->appends(request()->query());
        return View("estudiantes.index",$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['object'] = new Estudiante;
        $data["tipos_doc"] = User::get_document_type();       
        $data["ciudades"] =  DB::table("ciudades as c")
        ->select(["c.nombre", "c.id","dep.nombre as dep_nombre"])
        ->join("departamentos as dep", "dep.id","=","c.departamento_id")
        ->where("c.eliminado",0)->where("dep.eliminado",0)->where("c.pais_code","CO")->get();
        return view('estudiantes.modal-registro', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EstudianteRequest $request)
    {
        try {
            DB::beginTransaction();

            if(Estudiante::existe_decuemunto(null, request("tipo_documento"), request("documento_n"))){           
                return response()->json(["message"=>"Estudiante registrado.",
                "errors" =>["documento_n"=>["Tipo Documento y número ya existe"],
                ]], 422);
                
            }
            $objets = Util::updateOrCreate(Estudiante::class, $request); 
            
            $objets->codigo = $objets->id;

            if($objets->save()){
                DB::commit();
                $data['success'] = true;

            }else{
                DB::rollBack();
                $data['success'] = false;
            }
             
            $data['reload_page'] = true;    
            Util::addToastToData($data);
    
            return response()->crud($data);

        } catch (\Throwable $th) {
            DB::rollBack();
            $data['success'] = false; 
            $data['msg_error'] =$th;
            Util::addToastToData($data);
            return response()->crud($data);
        }

        



        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo"ok";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($estudiante)
    {
        $data['object'] = Estudiante::find($estudiante);      
        $data["tipos_doc"] = User::get_document_type();
        $data["ciudades"] =  DB::table("ciudades as c")
        ->select(["c.nombre", "c.id","dep.nombre as dep_nombre"])
        ->join("departamentos as dep", "dep.id","=","c.departamento_id")
        ->where("c.eliminado",0)->where("dep.eliminado",0)->where("c.pais_code","CO")->get();

        return view('estudiantes.modal-registro', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EstudianteRequest $request, $id)
    {
        if(Estudiante::existe_decuemunto($id, request("tipo_documento"), request("documento_n"))){           
            return response()->json(["message"=>"Estudiante registrado.",
            "errors" =>["documento_n"=>["Tipo Documento y número ya existe"],
            ]], 422);
           
        }
        $objets = Util::updateOrCreate(Estudiante::class, $request, $id);      
        $data['success'] = (boolean) $objets;
        $data['reload_page'] = true;        
        Util::addToastToData($data);

        return response()->crud($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['success'] = false;

        $estudiante = Estudiante::find($id);
        $data['success'] = $estudiante->delete();
        
        return response()->crud($data);

    }

    public function get_sugerencia()
    {
        $search = request("term");    
        $limit = 30;
        $data_sugerencia =  DB::table("estudiantes as e")->select(["e.documento_n as label","e.id as id"])->
        where(function($query) use ($search)
        {		 	
            $query->where('e.documento_n', 'like', "%".$search."%");      
        })
        ->whereNull('deleted_at')
        ->limit(30)->get()->toArray();
        

        $data =  DB::table("estudiantes as e")->select(["e.email as label","e.id as id"])->
        where(function($query) use ($search)
        {		 	
            $query->where('e.email', 'like', "%".$search."%");  
        })
        ->whereNull('deleted_at')
        ->limit(30)->get()->toArray();

        foreach ($data as $val) {
            $data_sugerencia[] = $val;
        }

        $data =  DB::table("estudiantes as e")->select([ DB::raw("CONCAT(nombre, ' ', segundo_nombre, ' ', apellido, ' ', apellido) as label"),"e.id as id"])->
        where(function($query) use ($search)
        { 
            $query->Where(DB::raw("CONCAT(nombre, ' ', segundo_nombre, ' ', apellido, ' ', apellido2)"), 'LIKE', "%".$search."%");
        })
        ->whereNull('deleted_at')
        ->limit(30)->get()->toArray();

        foreach ($data as $val) {
            $data_sugerencia[] = $val;
        }

        sort($data_sugerencia);
		
		//only return $limit suggestions
		if(count($data_sugerencia) > $limit)
		{
			$data_sugerencia = array_slice($suggestions, 0,$limit);
		}
        return response()->json($data_sugerencia,200);

    }

    
}
