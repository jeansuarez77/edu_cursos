<?php

namespace App\Http\Controllers;

use App\Http\Requests\AreaRequest;
use Illuminate\Http\Request;
use Storage;
use App\Models\Estudiante;
use App\Models\Curso;
use DB;
use Carbon;
class GeneradorController extends Controller
{
    public function get_certificado_curso(){
      $id = request('id');
      $curso_id = request('curso_id');

      $meses = array(
         "01"=>"ENERO",
         "02"=>"FEBRERO",
         "03"=>"MARZO",
         "04"=>"ABRIL",
         "05"=>"MAYO",
         "06"=>"JUNIO",
         "07"=>"JULIO",
         "08"=>"AGOSTO",
         "09"=>"SEPTIEMBRE",
         "10"=>"OCTUBRE",
         "11"=>"NOVIEMBRE",
         "12"=>"DICIEMBRE"
      );
      $curso = DB::table("aulas AS a")
      ->select(["fecha_finalizado","c.id","sede_id","duracion","vigencia_certificado","c.nombre_curso","c.prefijo", "consecutivo", "es_aprobado", "a.created_at"])
      ->join("cursos as c", "c.id","=","a.curso_id")
      ->join("estudiantes as e", "e.id","=","a.estudiante_id")
      ->where("e.id", $id)->where("c.id", $curso_id)->whereNotNull("fecha_finalizado")->first();     

      if($curso and $curso->es_aprobado == 1){
         $estudiante = DB::table("estudiantes AS e")
            ->select(["documento_n","tipo_documento","e.nombre","e.segundo_nombre","e.apellido", "e.apellido2", "c.nombre as c_nombre"])
            ->join("ciudades as c", "c.id","=","e.expedicion_ciudad_id")->where("e.id", $id)->first();

     
         $vigencia = date("d-m-Y",strtotime($curso->fecha_finalizado."+ ".$curso->vigencia_certificado." month"));
         $dia = date("d",strtotime($vigencia));
         $mes = $meses[date("m",strtotime($vigencia))];
         $anio = date("Y",strtotime($vigencia));
         $vigencia = $dia." DE ". $mes . " DE ".$anio;


         $dia = date("d",strtotime($curso->fecha_finalizado));
         $mes = $meses[date("m",strtotime($curso->fecha_finalizado))];
         $anio = date("Y",strtotime($curso->fecha_finalizado));

         $di2 = date("d",strtotime($curso->created_at));
         $realizado = $di2." al ".date("d")." de ". ucwords(strtolower($meses[date("m")])) . " de ".date("Y");
         
         $sede =   DB::table("ubicaciones AS u")
         ->select(["c.nombre as c_nombre"])
         ->join("ciudades as c", "c.id","=","u.ciudad_id")->where("u.id", $curso->sede_id)->first();

         $dado =  "DADO EN ".$sede->c_nombre." A LOS  ".date("d")." DÍAS DEL MES ". $meses[date("m")] . " DE ".date("Y");

         $data = [
            "nombre" => strtoupper($estudiante->nombre." ".$estudiante->segundo_nombre." ".$estudiante->apellido." ".$estudiante->apellido2),
            "numero_id" => $estudiante->documento_n,
            "tipo_id" => $estudiante->tipo_documento,
            "curso" => $curso->nombre_curso,
            "ciuda_exp" => strtoupper($estudiante->c_nombre),
            "vigencia" => $vigencia,
            "intensidad" => $curso->duracion,
            "codigo" => $curso->prefijo. "-".$curso->consecutivo,
            "dado" => strtoupper($dado),
            "realizado" => $realizado
         ];
         //return view("certificados.plantillas.plantilla-01",$data);
         $pdf = \PDF::loadView('certificados.plantillas.plantilla-01',$data);
         $pdf->setPaper( array(0,0,795.28,600)); //x inicio, y inicio, ancho final, alto final
         return $pdf->download('certificado '. $curso->prefijo.'-'.$curso->consecutivo.'.pdf');
         // return view("plantillas.plantilla-01");
      }else{
         $pdf = app('dompdf.wrapper');
         $pdf->setPaper( array(0,0,400,200)); 
         $pdf->loadHTML('<h1 style="color:red" >Certificado no disponible</h1>');
         return $pdf->download('certificado.pdf');
      }
    }

    public function index(){      
       return view("certificados.index");
    }
    public function get_lita_certificados(){ 
      $code = request('code');   
      if(is_numeric($code)){
         $estudiante = Estudiante::where("codigo", $code)->first();
         if($estudiante){         
            $cursos = DB::table("aulas AS a")
            ->select(["c.id","fecha_finalizado","vigencia_certificado","c.nombre_curso","c.prefijo", "consecutivo", "es_aprobado", "a.created_at"])
            ->join("cursos as c", "c.id","=","a.curso_id")
            ->join("estudiantes as e", "e.id","=","a.estudiante_id")->whereNotNull("fecha_finalizado")
            ->where("e.codigo", $code)->orderBy("c.id", "DESC")->get();

            $data["estudiante"]  =  $estudiante;
            $data["cursos"] = $cursos;
            return view("certificados.lista",$data);
         }
      }      
      echo"<h2 style='text-align: center;'>Estudiante <b> $code </b>no encontrado </h2>";      
   }
}
