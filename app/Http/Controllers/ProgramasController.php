<?php

namespace App\Http\Controllers;
use App\Http\Requests\ProgramaRequest;


use App\Models\Programa;
use App\Models\Area;
use App\Models\Curso;
use DB;

use Mappweb\Mappweb\Helpers\Util;

use Illuminate\Http\Request;

class ProgramasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['programas'] =  Programa::select('programas.*', 'areas.nombre as area')
            ->join('areas', 'areas.id', '=', 'programas.area_id')
            ->paginate("50");
        
        return view("programas.index",$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['object'] = new Programa;
        $data['areas'] = Area::all();
        return view('programas.modal-registro', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProgramaRequest $request)
    {
        $objets = Util::updateOrCreate(Programa::class, $request);
        
        $data['success'] =(boolean) $objets;
        $data['reload_page'] = true;
        
        Util::addToastToData($data);

        return response()->crud($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Programa  $programa
     * @return \Illuminate\Http\Response
     */
    public function show($programa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Programa  $programa
     * @return \Illuminate\Http\Response
     */
    public function edit($programa)
    {
        $data['object'] = Programa::find($programa);
        $data['areas'] = Area::all();
        return view('programas.modal-registro', $data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Programa  $programa
     * @return \Illuminate\Http\Response
     */
    public function update(ProgramaRequest $request,$id)
    {
        $objets = Util::updateOrCreate(Programa::class, $request, $id);
        
        $data['success'] =(boolean) $objets;

        Curso::where('programa_id', $id)
            ->update(['nombre_curso' => request('nombre')]);

        Util::addToastToData($data);

        return response()->crud($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Programa  $programa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
        
            
            Curso::where('programa_id', $id)->delete();

            $objets = Programa::find($id);

            $data['success'] = $objets->delete(); 
            DB::commit();
        
        } catch (\Throwable $th) {
            DB::rollBack();
            $data['success'] = false;
        }
        
        
        return response()->crud($data);
    }
}
