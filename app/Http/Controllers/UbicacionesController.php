<?php

namespace App\Http\Controllers;
use App\Models\Ubicacione;
use App\Http\Requests\UbicacionRequest;

use Mappweb\Mappweb\Helpers\Util;

use Illuminate\Http\Request;
use DB;


class UbicacionesController extends Controller
{
    public function index(){
        $busqueda = trim(request("buscar") ? request("buscar"): "");
        
        $query =  Ubicacione::select('ubicaciones.*','c.nombre as ciudad')
            ->join('ciudades as c', 'c.id', '=', 'ubicaciones.ciudad_id');
            
        if(!empty($busqueda)){
            $query->where(function($query) use ($busqueda)
            {		 	
                $query->where('ubicaciones.nombre', 'like', "%".$busqueda."%");  
                $query->orWhere('ubicaciones.direccion', 'like', "%".$busqueda."%");  
            });
        }
        $data['busqueda'] = $busqueda;
        $data['ubicaciones'] = $query->paginate()->appends(request()->query());
        return View("ubicaciones.index",$data);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['object'] = new Ubicacione;
        $data["ciudades"] =  DB::table("ciudades as c")
        ->select(["c.nombre", "c.id","dep.nombre as dep_nombre"])
        ->join("departamentos as dep", "dep.id","=","c.departamento_id")
        ->where("c.eliminado",0)->where("dep.eliminado",0)->where("c.pais_code","CO")->get();
        return view('ubicaciones.modal-registro', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UbicacionRequest $request)
    {
        
        $objets = Util::updateOrCreate(Ubicacione::class, $request);        
        $data['success'] = (boolean) $objets; 
        $data['reload_page'] = true;    
        Util::addToastToData($data);

        return response()->crud($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['object'] = Ubicacione::find($id);
        $data["ciudades"] =  DB::table("ciudades as c")
        ->select(["c.nombre", "c.id","dep.nombre as dep_nombre"])
        ->join("departamentos as dep", "dep.id","=","c.departamento_id")
        ->where("c.eliminado",0)->where("dep.eliminado",0)->where("c.pais_code","CO")->get();
        
        return view('ubicaciones.modal-registro', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UbicacionRequest $request, $id)
    {
        $objets = Util::updateOrCreate(Ubicacione::class, $request, $id);
        
        $data['success'] =(boolean) $objets;
        
        Util::addToastToData($data);
        $data['reload_page'] = true;    

        return response()->crud($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        

    }
  function get_ciudades()
  {
    $search = request("term");
    

    $data =  DB::table("ciudades as c")->select(["c.nombre as text", "c.id"])->
    where(function($query) use ($search)
    {		 	
        $query->where('c.nombre', 'like', "%".$search."%");
       // $query->orWhere('ct.name', 'ilike', "%$search%");        
    })
    ->where("c.eliminado",0)->limit(30)->get();

   return response()->json($data,200);

  }

    public function get_sugerencia()
    {
        $search = request("term");    
        $limit = 30;
        $data_sugerencia =  DB::table("ubicaciones as e")->select(["e.nombre as label","e.id as id"])->
        where(function($query) use ($search)
        {		 	
            $query->where('e.nombre', 'like', "%".$search."%");      
        })
        ->limit(30)->get()->toArray();
        

        $data =  DB::table("ubicaciones as e")->select(["e.prefijo as label","e.id as id"])->
        where(function($query) use ($search)
        {		 	
            $query->where('e.prefijo', 'like', "%".$search."%");  
        })
        ->limit(30)->get()->toArray();

        foreach ($data as $val) {
            $data_sugerencia[] = $val;
        }

        return response()->json($data_sugerencia,200);

    }
}
