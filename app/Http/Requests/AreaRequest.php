<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AreaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:3|unique:areas,nombre,'.$this->get('id'),
        ];
    }


    public function messages()
    {
        return [
            'nombre.required' => 'Nombre del area de estudio es obligatorio',
            'nombre.min'      => 'Minimo 3 caracteres',
            'nombre.unique'   => 'Ya existe un area con ese nombre'
        ];
    }

}
