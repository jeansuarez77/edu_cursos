<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CursoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'modalidad' => 'required',
            'duracion' => 'required|min:1',
            'fecha_inicio' => 'required|date',
            'fecha_fin' => 'required|date',
            'sede_id' => 'required',
            'tipo_jornada' => 'required',
            'vigencia_certificado' => 'required',
            'programa_id' => 'required'

        ];
    }

    public function messages()
    {
        return [
            'modalidad.required' => 'Modalidad del programa es requerida',
            'duracion.required'   => 'Intencidad de horas es requerida',
            'duracion.min'   => 'Intencidad de horas debe ser mayor a 0',

            'vigencia_certificado.required' => 'Fecha de inicio es requerida',
            'fecha_inicio.date' => 'Ingrese una fecha valida',
            'fecha_fin.required' => 'Fecha de inicio es requerida',
            'fecha_fin.date' => 'Ingrese una fecha valida',
            
            'sede_id.required'   => 'La sede es requerida',

            'vigencia_certificado.required' => 'La vigencia del certificado es requerida',
        ];
    }

}
