<?php

namespace App\Http\Requests;
use Laravel\Fortify\Rules\Password;

use Illuminate\Foundation\Http\FormRequest;

class EmpleadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:users,name,'.$this->get('id'),
            'email' => 'required|email|string|max:255|unique:users,email,'.$this->get('id'),
            'password' => ['required', 'string', new Password, 'confirmed'],
            //'password_confirmation' => 'confirmed',
        ];
    }

    public function messages()
    {
        return [
            'name.max' => 'Maximo 255 caracteres',
            'name.unique'   => 'Ya existe un usuario con ese nombre',

            'email.max' => 'Maximo 255 caracteres',
            'email.unique'   => 'Ya existe un usuario con ese Email',
            
            'password.confirmed'   => 'Las contraseñas no coinciden',

        ];
    }
}
