<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EstudianteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $input['tipo_documento'] = trim($input['tipo_documento']);
        $input['nombre'] = strtoupper(trim($input['nombre']));
        $input['segundo_nombre'] = strtoupper(trim($input['segundo_nombre']));
        $input['apellido'] = strtoupper(trim($input['apellido']));
        $input['apellido2'] = strtoupper(trim($input['apellido2']));
        $input['email'] = trim($input['email']);
        $this->replace($input);
        return [
            'tipo_documento' => 'required',
            'documento_n' => 'required',
            'nombre' => 'required|min:3',
            'apellido' => 'required',
            'apellido2' => 'required',
            "fecha_expedicion"=> 'required|date',
            "expedicion_ciudad_id"=> 'required',
            "email" => 'required|unique:estudiantes,email,'.$this->get('id'),
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El nombre obligatorio',
            'documento_n.required'   => 'El N° de documento obligatorio',
            'tipo_documento.required' => 'El Tipo de documento obligatorio',
            'apellido.required'   => 'El Primer Apellido son obligatorio',
            'apellido2.required'   => 'El segundo Apellidos es obligatorio',
            'fecha_expedicion.required' => 'La Fecha de expedición del documento es obligatoria',
            'fecha_expedicion.date' => 'Se requiere una fecha válida',
            'expedicion_ciudad_id.required' => 'El Lugar de expedición del documento es obligatoria',
            'email.required' => 'El correo es obligatorio',
        ];
    }

}
