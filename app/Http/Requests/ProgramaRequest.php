<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProgramaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|unique:programas,nombre,'.$this->get('id'),
            'codigo' => 'required|unique:programas,codigo,'.$this->get('id'),
            'descripcion' => 'required|min:3',
            'area_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El nombre del curso es obligatorio',
            'nombre.unique'   => 'Ya existe un curso con ese nombre',

            'codigo.required' => 'El codigo del curso es obligatorio',
            'codigo.unique'   => 'Ya existe un curso con ese codigo',

            'area_id.required' => 'El area de estudio es obligatorio',

            'descripcion.required' => 'La descripción del curso es obligatoria',
        ];
    }
}
