<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UbicacionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|unique:ubicaciones,nombre,'.$this->get('id'),
            'prefijo' => 'required|unique:ubicaciones,prefijo,'.$this->get('id'),
            'rango_inicio' => 'required',
            'direccion' => 'required',
            'ciudad_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El nombre de la sede es obligatorio',
            'nombre.unique'   => 'Ya existe una sede con ese nombre',

            'prefijo.required' => 'El prefijo de la sede es obligatorio',
            'prefijo.unique'   => 'Ya existe una sede con ese prefijo',

            'rango_inicio.required' => 'El rango de inicio es obligatorio',

            'direccion.required' => 'La direccion de la sede es obligatoria',

            'ciudad_id.required' => 'La ciudad es obligatoria'
        ];
    }
}
