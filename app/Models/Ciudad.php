<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ciudad extends Model
{    
    protected $table  = 'ciudades';

    protected $fillable = [       
        "departamento_id",
        "pais_code",
        "nombre",
        "cod_postal",
        "eliminado"
    ];

}