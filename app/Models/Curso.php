<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Curso extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'programa_id','nombre_curso','fecha_inicio','fecha_fin','modalidad','duracion','sede_id','tipo_jornada','vigencia_certificado','estado','prefijo','consecutivo','tipo_oferta'
    ];

    public static $VIGENCIA_CERTIFICADO = [
        '6' => '6 Meses',
        '7' => '7 Meses',
        '8'=>'8 Meses',
        '9'=>'9 Meses',
        '10'=>'10 Meses',
        '11'=>'11 Meses',
        '12'=>'1 Año',
        '24'=>'2 Año',

    ];

}
