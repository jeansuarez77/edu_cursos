<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estudiante extends Model
{    
    use SoftDeletes;
    protected $table  = 'estudiantes';
    
    protected $fillable = [
        "tipo_documento",
        "documento_n",
        "nombre",
        "segundo_nombre" ,
        "apellido" ,
        "apellido2",
        "pais_code" ,
        "departamento_id" ,
        "ciudad_id" ,
        "fecha_expedicion",
        "expedicion_ciudad_id",
        "direccion" ,
        "celular",
        "email"
    ];

    public static function existe_decuemunto($id, $tipo, $numero)
    {
        $query = Estudiante::where("tipo_documento",$tipo)->where("documento_n",trim($numero));
        if($id > 0)
            $query->where("id","!=", $id);
        
        return $query->exists();
    }
}