<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pais extends Model
{
    
    protected $table  = 'paises';
    public $timestamps = false;
    protected $keyType = 'string';
    protected $primaryKey = 'codigo';
    
    protected $fillable = [
        "codigo",
        "nombre",
        "eliminado"
    ];

}