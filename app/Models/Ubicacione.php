<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ubicacione extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $fillable = [
        'nombre','direccion','rango_inicio','prefijo','ciudad_id'
    ];
}
