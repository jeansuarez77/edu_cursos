<?php

namespace App\Providers;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            // Add some items to the menu...
            if(\Auth::check()){
                $event->menu->addAfter('main_navigation', 
                [
                    'key' => 'home',
                    'text' => 'home',
                    'url' => '',
                    'icon' => 'fas fa-fw fa-layer-group',
                ],

                [
                    'key' => 'sedes',
                    'text' => 'Sedes',
                    'url' => 'sedes',
                    'icon' => 'fas fa-house-user',
                ],
                [
                    'key' => 'estudiantes',
                    'text' => 'Estudiantes',
                    'url' => 'estudiantes',
                    'icon' => 'fas fa-fw fa-users',
                ],

                [
                    'key' => 'programas',
                    'text' => 'Ofertas',
                    'url' => 'programas',
                    'icon' => 'fas fa-book', 
                ],
                [
                    'key' => 'cursos',
                    'text' => 'Cursos y diplomados',
                    'url' => 'cursos',
                    'icon' => 'fas fa-laptop-house', 
                ],
                [
                    'key' => 'certificados',
                    'text' => 'Certificados',
                    'url' => 'certificados',
                    'icon' => 'fas fa-certificate', 
                ],
                [
                    'key' => 'empleados',
                    'text' => 'Empleados',
                    'url' => 'empleados',
                    'icon' => 'fas fa-user', 
                ]
                );
            }
        });
    }
}
