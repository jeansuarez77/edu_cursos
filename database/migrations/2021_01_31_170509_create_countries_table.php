<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paises', function (Blueprint $table) {            
            $table->string('codigo',5)->primary();
            $table->string('nombre');
            $table->bigInteger('eliminado')->index()->default(0);            
        });
        Schema::create('departamentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pais_code',5);
            $table->string('nombre');
            $table->bigInteger('eliminado')->index()->default(0);
            $table->foreign('pais_code')->references('codigo')->on('paises');
        });
        Schema::create('ciudades', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->bigInteger('departamento_id')->unsigned();
            $table->string('pais_code',5);
            $table->string('nombre');
            $table->string('cod_postal',20)->nullable()->index();
            $table->bigInteger('eliminado')->index()->default(0);            
            $table->foreign('pais_code')->references('codigo')->on('paises');
            $table->foreign('departamento_id')->references('id')->on('departamentos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ciudades');
        Schema::dropIfExists('departamentos');
        Schema::dropIfExists('paises');        
       
    }
}
