<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('estudiantes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tipo_documento', 10);
            $table->string('documento_n', 20)->index();
            $table->string('nombre', 80);            
            $table->string('segundo_nombre', 80)->nullable();
            $table->string('apellido', 90);
            $table->string('apellido2', 90);
            $table->string('pais_code',5)->nullable()->default("CO");
            $table->bigInteger('departamento_id')->nullable()->unsigned();
            $table->bigInteger('ciudad_id')->nullable()->unsigned();
            $table->date('fecha_expedicion')->nullable();
            $table->bigInteger('expedicion_ciudad_id')->unsigned()->nullable();
            $table->string('direccion', 255)->nullable();
            $table->string('celular', 100)->nullable();  
            $table->string('email', 150)->index()->unique()->nullable();  
            $table->bigInteger('codigo')->unique()->index()->nullable()->default(null);
            
            $table->softDeletes();
            $table->foreign('pais_code')->references('codigo')->on('paises');
            $table->foreign('departamento_id')->references('id')->on('departamentos');
            $table->foreign('ciudad_id')->references('id')->on('ciudades');
            $table->timestamps();
           
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
