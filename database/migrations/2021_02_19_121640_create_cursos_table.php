<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('modalidad');
            $table->double('duracion'); 
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->bigInteger('sede_id')->nullable()->unsigned();
            $table->string('tipo_jornada'); 
            $table->integer('estado')->default("0");
            $table->string('vigencia_certificado');
            $table->string('nombre_curso');
            $table->string('prefijo');
            $table->string('consecutivo')->unique()->index();
            $table->bigInteger('programa_id')->unsigned(); 
            $table->integer('tipo_oferta')->default("0");
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('programa_id')->references('id')->on('programas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('sede_id')->references('id')->on('ubicaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('cursos');
    }
}
