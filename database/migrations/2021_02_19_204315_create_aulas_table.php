<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAulasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aulas', function (Blueprint $table) {
           
            $table->bigIncrements('id');
            $table->bigInteger('estudiante_id')->unsigned(); 
            $table->bigInteger('curso_id')->unsigned(); 
            $table->boolean('es_aprobado')->default(0); // 0 pendiente, 1 aprobado, 2 no aprobado
            $table->dateTime('fecha_finalizado')->nullable();            
            $table->foreign('estudiante_id')->references('id')->on('estudiantes')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('curso_id')->references('id')->on('cursos')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('aulas');
    }
}
