<?php

use Illuminate\Database\Seeder;
use DB as DBS;
class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = base_path('database');
        DBS::unprepared(file_get_contents($path."/paises.sql"));
        $this->command->info('Paises insert');
        DBS::unprepared(file_get_contents($path."/departamentos.sql"));
        $this->command->info('departamentos insert');
        DBS::unprepared(file_get_contents($path."/ciudades.sql"));
        $this->command->info('ciudades insert');       
    }
}
