ALTER TABLE `edu_cursos`.`estudiantes` 
ADD UNIQUE INDEX `n_doc_inx` (`documento_n` ASC, `tipo_documento` ASC) ;
;

ALTER TABLE `edu_cursos`.`estudiantes` 
ADD INDEX `estudiantes_names_index` (`nombre` ASC, `segundo_nombre` ASC, `apellido` ASC, `apellido2` ASC) ;

ALTER TABLE `edu_cursos`.`estudiantes` 
CHANGE COLUMN `codigo` `codigo` INT(6) ZEROFILL  NULL ;

