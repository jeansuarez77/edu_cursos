


<div class="modal fade" id="mdoal-add-areas" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-success">
          <h4 class="modal-title">Gestión de Areas</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form id="formArea"  class="save-ajax" method="{{$object->exists? 'POST':'POST'}}" data-has-files='true' action="{{ $object->exists? route('areas.update', ['area' => $object->id]):route('areas.store') }}">
            @csrf
            @if($object->exists)
                <input hidden name="_method" value="PUT">
            @endif
            <div class="modal-body">
                
                <div class="card-body row">
                  
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="nombre">Nombre del Area de estudio: <span class="text-danger">*</span> </label>
                            <input type="text" class="form-control text-capitalize" name="nombre" id="nombre" placeholder="Ingrese nombre area de estudio" required>
                            <div data-feedback="nombre" class="text-danger">                           
                            </div>
                        </div>
                    </div>

                </div>
                    <!-- /.card-body -->

            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-submit btn-primary">Guardar</button>
            </div>
        </form>

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>


  <script>
  $( document ).ready(function() {
  // Handler for .ready() called.
});

 
  </script>