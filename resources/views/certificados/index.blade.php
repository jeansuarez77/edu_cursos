@extends('adminlte::master')

@section('title', 'Certificados')
<br>
<div id="preloader" style="text-align: center;padding-top: 80px;padding-bottom: 100px;">
    <div   class="preloader" ></div>
    <p><b> Cargando...</b></p>
</div>


<div id="contents" class="row" style="display:none">
    <div class="login-box col-md-5 offset-md-3">
        <!-- /.login-logo -->
        <div class="card card-outline card-success">
            <div class="card-header text-center">
                <a href="javascript:;" class="h1">Consultar<b> Certificados</b></a>
            </div>
            <div class="card-body">
                <p class="login-box-msg">Ingrese sus datos para consultar certificados.</p>                    
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="code">Código: <span class="text-danger">*</span> </label>
                            <input outocomplete="off" type="text" id="code" class="form-control"
                                placeholder="Código del estudiante">
                        </div>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-12 text-center">
                            <button type="button" id="btn-send" class="btn btn-primary btn-block">Consultar</button>
                        </div>
                        <!-- /.col -->
                    </div>

            </div>
            <!-- /.card-body -->
        </div>
        <div id="container">
            
        </div>
    </div>
</div>
<style>
.preloader {  
  width: 130px;
  height: 130px;
  border: 10px solid #94cea18c;
  border-top: 10px solid #28a745;
  border-radius: 50%;
  animation-name: girar;
  animation-duration: 0.9s;
  animation-iteration-count: infinite;
  animation-timing-function: linear;
  margin-left: auto;
  margin-right: auto;
}

@keyframes girar {
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
}

</style>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
    $("#preloader").hide();
    $("#contents").show();
    $("#btn-send").click(function(e) {

        let data = {            
            code: $("#code").val()       
        };
        $.get("<?=url("certificados-digitales/get_lita_certificados")?>", data, (resp) => {
            $("#container").html(resp);
        });
    });
});
</script>