<div class="card card-outline card-success">

    <div class="card-body">
      <div>
        <h2 style="font-size: 25px;" >{{$estudiante->nombre ." ".$estudiante->segundo_nombre." ".$estudiante->apellido." ".$estudiante->apellido2}}</h2>
      </div>
        <table id="tabla-curos" class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Código</th>
                    <th scope="col">Curso</th>
                    <th scope="col">Fecha de registro</th>
                    <th scope="col">Vigencia</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($cursos as $c) { ?>
                <tr>
                    <th scope="row">{{$c->prefijo."-".$c->consecutivo}}</th>
                    <td>{{$c->nombre_curso}} </td>
                    <td scope="row">{{date("d-m-Y",strtotime($c->created_at))}}</td>
                    <td scope="row"><?= date("d-m-Y",strtotime(date($c->fecha_finalizado)."+ ".$c->vigencia_certificado." month"))?></td>
                    <td>
                      <?php if($c->es_aprobado == 0) {?>
                        <span class="badge badge-warning">Pendiente</span>
                      <?php } elseif($c->es_aprobado == 1) {?>
                        <a href="<?=url("certificados-digitales/get_certificado_curso?curso_id=".$c->id."&id=".$estudiante->id)?>"
                            class="btn btn-sm btn-info" title="Descargar certificado"><i class="fa fa-download" aria-hidden="true"></i></a>
                      <?php } else {?>
                        <span class="badge badge-danger">No aprobado</span>
                      <?php } ?>
                    </td>                    
                </tr>
                <?php } ?>

                <?php if(count($cursos) == 0) {?>
                  <tr>
                    <th COLSPAN="5" class="text-center" > No hay registro</th>                    
                  </td>                    
                </tr>
                <?php } ?>

                
            </tbody>
        </table>
    </div>
</div>

<script>$('#tabla-curos').DataTable();</script>