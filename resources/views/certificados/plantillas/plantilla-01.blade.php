<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Certificado</title>
        <style>
         
         body{
            background-image: url('https://marketplace-facilpos.s3.us-east-2.amazonaws.com/img/PLANTILLA+CERTIFICADOS_NEIVA-BOGOTA.png') ;
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: center center;
            background-size: 100%;
            margin: 0px;
        }
        @page {margin:0px;}
        .contenedor{
            text-align: center;
        }
   
        
    </style>
    </head>
    <body >
        <div style="
        text-align: right;
        text-align: right;
        padding-right: 90px;
        padding-top: 70px"><span><b>{{$codigo}}</b></span></div>
        
        <div style="text-align: center;padding-top: 230px;">
            <h1 style="font-size: 28px;"><b>{{$nombre}}</b></h1>
            <h2 style="font-size: 22px;">{{$tipo_id}}. {{$numero_id}} DE {{$ciuda_exp}}</h1>
            <p style="margin: 0px;" >ASISTIÓ Y ADQUIRIÓ CONOCIMIENTOS Y HABILIDADES EN EL CURSO TALLER DE</p>
            <h1 style="font-size: 32px; margin-top: 4px; argin-bottom: 10px;"><b>{{$curso}}</b></h1>
            <p style="margin: 0px;" >Realizado los días  {{$realizado}}, con una intensidad de {{$intensidad}} horas.</p>
            <p style="margin: 0px;">CERTIFICACIÓN VIGENTE HASTA EL DÍA {{$vigencia}}</p>
            <p><b>{{$dado}}</b></p>
        </div>       
    </body>
</html>