@extends('adminlte::page')

@section('title', 'Cursos')

@section('css')
<link rel="stylesheet" href="css/jquery-ui.min.css">
@endsection

@section('content_header')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="/">Inicio</a></li>
                    <li class="breadcrumb-item active">Cursos y diplomados</li>
                </ol>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@stop

@section('content')

    <div class="card card-outline card-success">
        <div class="card-header">
            <h3 class="card-title font-weight-bold">Gestión de cursos y diplomados</h3>

            <div class="card-tools">
                <div class="btn-group">
                    <a  href="{{ route('cursos.create') }}" class="btn btn-success open-modal" >
                        <i class="fas fa-plus"></i> Crear
                    </a>
                </div>

                
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body p-0">
            
            <form action="{{ route('cursos.index') }}" method="get">
                <div class="row p-1">
                    <div class="col-md-4 col-sm-6 col-lg-2"> 
                        <div class="form-group input-group-sm">
                            {!! Form::select('tipo_oferta', array('' => 'Todos','0' => 'Cursos', '1' => 'Diplomados'), $tipo_oferta,['class'=>'form-control','id' => 'tipo_oferta']); !!}
                           
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="input-group input-group-sm">
                            <input id="buscador_curso" name="buscador_curso" type="text" value="{{$buscador_curso}}"
                                placeholder="Ingresa un palabra para Buscar" class="form-control">
                            <span class="input-group-append">
                                <button type="submit" id="btn-subtmit-curso" class="btn btn-info btn-flat">Buscar</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <button type="button" onclick="redireccionar()" class="btn btn-info btn-info btn-sm">Restablecer búsqueda</button>
                    </div>
            </form>
            </div>

            <div class="card-body table-responsive p-0">

            <table class="table table-hover text-nowrap mt-2 ">
                <thead>
                    <tr>
                        <th style="width: 10px">Nombre</th>
                        <th>Tipo</th>
                        <th>Fecha inicio</th>
                        <th>Fecha finalización</th>
                        <th>Modalidad</th>
                        <th>Tipo de Jornada</th>
                        <th>Sede</th>
                        <th>Intencidad horas</th>
                        <th style="width: 200px">Estado</th>
                        <th style="width: 100px">Opc</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($cursos as $curso)
                    <tr>
                        <td class="font-weight-bold"> {{ $curso->programa }} </td>
                        <td class="font-weight-bold"> {{ $curso->tipo_oferta==1?"Diplomado":"Curso"}} </td>
                        <td> {{ date('d-M-Y',strtotime($curso->fecha_inicio)) }} </td>
                        <td> {{ date("d-M-y",strtotime($curso->fecha_fin)) }} </td>
                        <td> {{ $curso->modalidad }} </td>
                        <td> {{ $curso->tipo_jornada }} </td>
                        <td> {{ $curso->sede }} </td>
                        <td> {{ $curso->duracion }} </td>
                        <td> <span class="float-left badge bg-{{$curso->estado==0?'success':($curso->estado==1?'info':'danger')}}">{{ $curso->estado== 0?'Abierto':($curso->estado== 1?'Finalizado':'Cerrado') }}  </span></td>
                        <td>
                                <a class="open-modal bg-info btn-sm" href="{{route('cursos.edit',['curso' => $curso->id])}}" >
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                            
                                <a class="open-modal bg-success btn-sm ml-1" title="Estudiantes" href="{{route('cursos.get-estudiantes',['curso' => $curso->id])}}" >
                                    <i class="fas fa-fw fa-users "></i>
                                </a>
                            
                        </td>
                    </tr>
                    @endforeach

                    @if ($cursos->count()==0)
                        <tr>
                            <td colspan="9" class="text-center"> No hay historial</td>
                        </tr>
                    @endif

                </tbody>
            </table>
        </div>

        <div class="card-footer clearfix">
            {{ $cursos->render("pagination::bootstrap-4") }}
        </div>

        <!-- /.card-body -->
    </div>

    <div id="modal-add"></div>

    @stop

    @section('js')

    <script src="js/jquery-ui.min.js"></script>

    <script>
        function redireccionar(){
            window.location='{{route("cursos.index")}}';
        }

        $(document).ready(function() {
            $("#buscador_curso").autocomplete({
                source: '<?php echo route("cursos.get-sugerencia"); ?>',
                delay: 300,
                autoFocus: false,
                minLength: 1,
                select: function(event, ui) {
                    event.preventDefault();
                    $("#buscador_curso").val(ui.item.label);
                    $("#btn-subtmit-curso").click();

                }
            });

        });

    </script>
@endsection
