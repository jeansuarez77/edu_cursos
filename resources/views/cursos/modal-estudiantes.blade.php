<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="plugins/checkbox/style.css">



<style>
ul.ui-autocomplete {
    z-index: 1100;
}


</style>


<div class="modal fade" id="modal-lg" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header bg-success">
          <h4 class="modal-title">Estudiantes Registrados</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form id="formCursos"  class="save-ajax" method="{{'POST'}}" data-has-files='true' action="{{ route('cursos.update', ['curso' => '2'])}}">
            @csrf
            
            <div class="card-body">
                    
                <div class="row mb-3">
                  <div class="col-md-6">
                    <div class="input-group input-group-md">
                        <input id="buscador" name="buscar" type="text" value=""
                            placeholder="Buscar Estudiantes para agregar al curso" class="form-control">
                        <span class="input-group-append">
                            <button type="button" class="btn btn-info btn-flat">Buscar</button>
                        </span>
                    </div>
                  </div>
                </div>
                <div  class="table-responsive">
                <table id="tabla_estudiantes" class="table table-bordered table-striped mt-3">
                  <thead>
                  <tr>
                    <th></th>
                    <th>Tipo documento</th>
                    <th>Documento</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Email</th>
                    <th>Telefono</th>
                    <th>Estado</th>
                    <th>
                      Opc
                    </th>


                  </tr>
                  </thead>
                  <tbody>
                    @foreach ($object as $item)
                        <tr id="row_{{$item->id}}">
                          <td>
                              <div class="form-group clearfix">
                                <div class="icheck-success d-inline">
                                  <input type="checkbox" name="check_estudiantes[]" value="{{$item->id}}"  id="checkboxSuccess{{$item->id}}">
                                  <label for="checkboxSuccess{{$item->id}}">
                                  </label>
                              </div>
                          </td>
                          <td> {{ $item->tipo_documento }}</td>
                          <td> {{ $item->documento_n }}</td>
                          <td> {{ $item->nombre.' '. $item->segundo_nombre }}</td>
                          <td> {{ $item->apellido.' '. $item->apellido2 }}</td>
                          <td> {{ $item->email }}</td>
                          <td> {{ $item->celular }}</td>

                          <td>
                            <span class="badge badge-{{$item->es_aprobado?'success':'warning'}}">
                              {{$item->es_aprobado?'Certificado':'En Curso'}}
                            </span>
                          </td>

                          <td>
                            <a class="bg-danger btn-sm ml-1" href="javascript:eliminar_registro('{{$item->id}}')">
                              <i class="fas fa-trash-alt"></i>
                            </a>
                          </td>
                        
                        </tr>
                    @endforeach
                  </tfoot>
                </table>
              </div>
              </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-info" onclick="aprobar_todos()">Certificar estudiantes marcados</button>
            </div>
        </form>

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <script src="plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
  <script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
  <script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
  <script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
  <script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
  <script src="plugins/jszip/jszip.min.js"></script>
  <script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
  <script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
  <script src="js/jquery-ui.min.js"></script>
  <script src="/plugins/sweetalert/sweetalert2.min.js"></script>
  
  <script>
    $(function () {
      $("#tabla_estudiantes").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["excel", "pdf", "print", "colvis"],
      }).buttons().container().appendTo('#tabla_estudiantes_wrapper .col-md-6:eq(0)');
    
      $("#buscador").autocomplete({
        source: '<?php echo route("estudiantes.get-sugerencia"); ?>',
        delay: 300,
        autoFocus: false,
        minLength: 1,
        select: function(event, ui) {
            event.preventDefault();
            agregar_estudiantes(ui.item.id);
            $("#buscador").val('');

        }
      });
      

    });
  </script>

  <script>
    function agregar_estudiantes(id){
      var table = $('#tabla_estudiantes').DataTable();
      if(id){
        data = {
          _token: "{{ csrf_token() }}",
          estudiante_id: id,
          curso_id : "{{$curso_id}}",
        }
        $.post("{{ route('cursos.add-estudiantes')}}",data,
          function (reponse) {
            if(reponse.success){
              object = reponse.object;

              var opc = `<a class="bg-danger btn-sm ml-1" href="javascript:eliminar_registro('${object.id_aula}',this)">
                      <i class="fas fa-trash-alt"></i>
                    </a>`;
              var estado = "<span class='badge badge-warning'> En Curso </span>";

              var check = `<div class="form-group clearfix">
                                <div class="icheck-success d-inline">
                                  <input type="checkbox" name="check_estudiantes[]" value="${object.id_aula}"  id="checkboxSuccess${object.id_aula}">
                                  <label for="checkboxSuccess${object.id_aula}">
                                  </label>
                              </div>`;
                            
              table.row.add( [
                check,
                object.tipo_documento,
                object.documento_n,
                object.nombre +' '+object.segundo_nombre,
                object.apellido +' '+object.apellido2,
                object.email,
                object.celular,
                estado,
                opc,
              ] ).node().id_aula = 'row_'+object.id_aula;
              table.draw( false );
              toastr.success('Estudiante agregado al curso exitosamente');

            }else if(reponse.existe){
              toastr.error('El estudiante seleccionado ya esta registrado en este curso');
            }else{
              toastr.error('No fue posible agregar al estudiante');
            }

          },
          "json"
        ).fail(function() {
          toastr.error('Error al procesar la solicitud')
        });
      }else{
        toastr.error('Identificador del estudiante no encontrado');

      }
     
    }
  function eliminar_registro(id){
    var table = $('#tabla_estudiantes').DataTable();


    data = {
        "id": id,
        "_token": "{{ csrf_token() }}",
    },
    swal({
      title: "Esta seguro de eliminar el registro?",
      text: "",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true
    }).then(function(result) {
        if(result.value){
            $.ajax(
            {
                url:"aulas/"+id,
                type: 'DELETE',
                data:data,
                success :function(reponse){
                    if(reponse.success){
                        toastr.success('Registro eliminado exitosamente');
                        table.row('#row_'+id).remove().draw();


                    }else{
                        toastr.error('No fue posible eliminar el registro');
                    }
                },
                error: function (response) {
                    toastr.error('Error al procesar la solicitud')
                }
            });
        }

    });
  }

  </script>
      
  <script>
    function aprobar_todos(){

      let data_check = [];

      $('input[name="check_estudiantes[]"]:checked').each(function(){
        data_check.push(this.value);
      });

      data = {
        "curso_id" : "{{$curso_id}}",
        "data": JSON.stringify(data_check),
        "_token": "{{ csrf_token() }}",
      };

      if(data_check.length > 0){

        $.post("{{ route('cursos.aprobar-estudiantes')}}",data,
        function (reponse) {
          if(reponse.success){
            toastr.success('Estudiantes aprobados');
            location.reload();
          }else{
            toastr.error('No fue posible aprobar estudiantes');
          }
        },
        "json"
        ).fail(function() {
          toastr.error('Error al procesar la solicitud')
        });

      }else{
        toastr.error('No hay datos seleccionados');
      }

      
    }
  </script>
