<link rel="stylesheet" href="css/select2.min.css">

<div class="modal fade" id="modal-lg" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-success">
          <h4 class="modal-title">Gestión de cursos y diplomados</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form id="formCursos"  class="save-ajax" method="{{$object->exists? 'POST':'POST'}}" data-has-files='true' action="{{ $object->exists? route('cursos.update', ['curso' => $object->id]):route('cursos.store') }}">
            @csrf
            @if($object->exists)
                <input type="hidden" value="{{$object->exists ? $object->id: '0'}}"  id="id" name="id">
                <input hidden name="_method" value="PUT">
            @endif
            <div class="modal-body">
                
                <div class="card-body row">

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Programa: <span class="text-danger">*</span> </label>
                            <select name="programa_id" class="form-control">
                                @foreach ($programas as $programa)
                                    @if ($programa->id == $object->programa_id)

                                        <option value="{{$programa->id}}" selected>{{$programa->nombre}}</option>
                                    @else
                                        <option value="{{$programa->id}}">{{$programa->nombre}}</option>
                                    @endif

                                @endforeach
                            </select>
                            <div data-feedback="programa_id" class="text-danger">                           
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="fecha_inicio">Fecha de inicio: <span
                                    class="text-danger">*</span></label>
                            <input type="date" class="form-control" name="fecha_inicio" id="fecha_inicio"
                                placeholder="Ingrese fecha de inicio" value="{{ $object->fecha_inicio }}">
                            <div data-feedback="fecha_inicio" class="text-danger">
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="fecha_fin">Fecha de finalización: <span
                                    class="text-danger">*</span></label>
                            <input type="date" class="form-control" name="fecha_fin" id="fecha_fin"
                                placeholder="Ingrese fecha finalización" value="{{ $object->fecha_fin }}">
                            <div data-feedback="fecha_fin" class="text-danger">
                            </div>

                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="nombre">Intencidad de horas: <span class="text-danger">*</span> </label>
                            <input type="number" class="form-control" name="duracion" id="duracion" placeholder="Ingrese la intencidad de horas"  value="{{ $object->duracion }}">
                            <div data-feedback="duracion" class="text-danger">                           
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Modalidad: <span class="text-danger">*</span> </label>
                            {!! Form::select('modalidad', array('Presencial' => 'Presencial', 'Virtual' => 'Virtual'), $object->modalidad,['class'=>'form-control','id' => 'modalidad']); !!}
                            <div data-feedback="modalidad" class="text-danger">                           
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Tipo de Jornada: <span class="text-danger">*</span> </label>
                            {!! Form::select('tipo_jornada', array('Diurna' => 'Diurna', 'Nocturna' => 'Nocturna','Mixta' => 'Mixta'), $object->tipo_jornada,['class'=>'form-control','id' => 'modalidad']); !!}
                            <div data-feedback="tipo_jornada" class="text-danger">                           
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Tipo de oferta: <span class="text-danger">*</span> </label>
                            {!! Form::select('tipo_oferta', array('0' => 'Curso', '1' => 'Diplomados'), $object->tipo_oferta,['class'=>'form-control','id' => 'tipo_oferta']); !!}
                            <div data-feedback="tipo_oferta" class="text-danger">                           
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="sede_id">Sede de realización: <span
                                    class="text-danger">*</span></label>

                            <select lass="form-control select2" style="width: 100%;" name="sede_id" id="sede_id" >
                            <option value="" >-- Seleccione --</option>    
                            <?php  foreach ($sedes as $c): ?>
                                <option value="<?=$c->id?>" <?= $object->sede_id  == $c->id ? "selected": ""?>>
                                    <?=$c->nombre. " - ".$c->prefijo?>
                                </option>
                            <?php endforeach; ?>
                            </select>
                            
                            <div data-feedback="sede_id" class="text-danger">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="vigencia_certificado">Vigencia de certificación: <span
                                    class="text-danger">*</span></label>
                                    {!! Form::select('vigencia_certificado', $vigencia_certificado, $object->vigencia_certificado,['class'=>'form-control','id' => 'vigencia_certificado']); !!}
                            <div data-feedback="vigencia_certificado" class="text-danger">
                            </div>

                        </div>
                    </div>

                    @if ($object->exists)
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="estado" name ="estado" value="Finalizado">
                                    <label for="estado" class="custom-control-label">Marcar como finalizado?</label>
                                </div>
                            </div>
                        </div>  
                    @endif
                   
                </div>
                    <!-- /.card-body -->

            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </form>

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <script src="js/select2.full.min.js"></script>

  <script>
    jQuery(document).ready(function() {
        $("#sede_id").select2({
            placeholder: "Seleccione una Sede",
            theme: 'bootstrap4',
        });
    });
      

  </script>