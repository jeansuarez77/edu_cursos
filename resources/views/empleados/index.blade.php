@extends('adminlte::page')
@section('title', 'Empleados')

@section('css')
<link rel="stylesheet" href="css/jquery-ui.min.css">
@endsection

@section('content_header')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="/">Inicio</a></li>
                    <li class="breadcrumb-item active">Empleados</li>
                </ol>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@stop


@section('content')


<!-- /.card -->

<div class="card card-outline card-success">
    <div class="card-header">
        <h3 class="card-title font-weight-bold">Gestión de Empleados <span class="badge badge-success">
                {{  $empleados->count()}}</span></h3>

        <div class="card-tools">
            <div class="btn-group">
                <a href="{{ route('empleados.create') }}" class="btn btn-success open-modal">
                    <i class="fas fa-plus"></i> Crear Empleado
                </a>
            </div>


        </div>
    </div>
    <table class="table table-hover text-nowrap">
        <thead>
            <tr>
                <th style="width: 100px">id</th>
                <th>Nombres</th>
                <th>Correos</th>
                <th>Fecha Creación </th>
                <th style="width: 100px">Opc</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($empleados as $empleado)
            <tr id="row_{{$empleado->id}}">
                <td> {{ $empleado->id }} </td>

                <td> {{ $empleado->name }} </td>
                <td> {{ $empleado->email }} </td>
                <td> {{ date("d-M-y",strtotime($empleado->created_at)) }} </td>
                <td>
                    <a class="open-modal bg-info btn-sm"
                        href="{{route('empleados.edit',['empleado' => $empleado->id])}}">
                        <i class="fas fa-pencil-alt"></i>
                    </a>

                    <a class="bg-danger btn-sm ml-1" href="javascript:eliminar_registro('{{$empleado->id}}')">
                        <i class="fas fa-trash-alt"></i>
                    </a>

                </td>
            </tr>
            @endforeach

            @if ($empleados->count()==0)
            <tr>
                <td colspan="5" class="text-center"> No hay empleados registrados</td>
            </tr>
            @endif

        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    {{ $empleados->render("pagination::bootstrap-4") }}
</div>


<!-- /.card-body -->
</div>

<div id="modal-add"></div>


@stop
@section('js')
<script src="/plugins/sweetalert/sweetalert2.min.js"></script>
<script>

    function eliminar_registro(id){

        data = {
            "id": id,
            "_token": "{{ csrf_token() }}",
        },
        swal({
        title: "",
        text: "Esta seguro de eliminar el registro?",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
        }).then(function(result) {
            if(result.value){
                $.ajax(
                {
                    url:"empleados/"+id,
                    type: 'DELETE',
                    data:data,
                    success :function(reponse){
                        if(reponse.success){
                            toastr.success('Empleado eliminado exitosamente');
                            $("#row_"+id).remove();

                        }else{
                            toastr.error('No fue posible eliminar el empleado');
                        }
                    },
                    error: function (response) {
                        toastr.error('Error al procesar la solicitud')
                    }
                });
            }

        });
    }
    
</script>
@endsection