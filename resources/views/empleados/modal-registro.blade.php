


<div class="modal fade" id="mdoal-add-empleados" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header bg-success">
          <h4 class="modal-title">Gestión de Empleados</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form id="formArea"  class="save-ajax" method="{{$object->exists? 'POST':'POST'}}" data-has-files='true' action="{{ $object->exists? route('empleados.update', ['empleado' => $object->id]):route('empleados.store') }}">
            @csrf
            @if($object->exists)
                <input hidden name="_method" value="PUT">
            @endif
            <div class="modal-body">
                
                <div class="card-body row">
                  
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name">Nombre completo: <span class="text-danger">*</span> </label>
                            <input type="text" class="form-control text-capitalize" name="name" id="name" placeholder="Ingrese nombre de usuario" required value="{{ $object->name }}">
                            <div data-feedback="name" class="text-danger feedback_error">                           
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="email">Email: <span class="text-danger">*</span> </label>
                            <input type="text" class="form-control text-capitalize" name="email" id="email" placeholder="Ingrese Correo electronico" required value="{{ $object->email }}">
                            <div data-feedback="email" class="text-danger feedback_error">                           
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="password">Contraseña: <span class="text-danger">*</span> </label>
                            <input type="password" class="form-control text-capitalize" name="password" id="password" placeholder="Ingrese contraseña" >
                            <div data-feedback="password" class="text-danger feedback_error">                           
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="password_confirmation">Confirmar contraseña: <span class="text-danger">*</span> </label>
                            <input type="password" class="form-control text-capitalize" name="password_confirmation" id="password_confirmation" placeholder="Confirmar contraseña" >
                            
                        </div>
                    </div>
                    

                </div>
                    <!-- /.card-body -->

            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-submit btn-primary">Guardar</button>
            </div>
        </form>

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>


  <script>
  $( document ).ready(function() {
  // Handler for .ready() called.
});

 
  </script>