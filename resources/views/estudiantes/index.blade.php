@extends('adminlte::page')

@section('css')
<link rel="stylesheet" href="css/jquery-ui.min.css">
@endsection

@section('content_header')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="/">Inicio</a></li>
                    <li class="breadcrumb-item active">Estudiantes</li>
                </ol>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@stop


@section('content')


<!-- /.card -->

<div class="card card-outline card-success">
    <div class="card-header">
        <h3 class="card-title font-weight-bold">Gestión de estudiantes <span class="badge badge-success">
                {{  $estudiantes->total()}}</span></h3>

        <div class="card-tools">
            <div class="btn-group">
                <a href="{{ route('estudiantes.create') }}" class="btn btn-success open-modal">
                    <i class="fas fa-plus"></i> Crear estudiante
                </a>
            </div>


        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0">
        <form action="{{ route('estudiantes.index') }}" method="get">
            <div class="row p-1">
                <duv class="col-md-3">
                    <div class="input-group input-group-sm">
                        <input id="buscador" name="buscar" type="text" value="{{$busqueda}}"
                            placeholder="Ingresa un palabra para Buscar" class="form-control">
                        <span class="input-group-append">
                            <button type="submit" id="btn-subtmit" class="btn btn-info btn-flat">Buscar</button>
                        </span>
                    </div>
                </duv>
                <duv class="col-md-3">
                    <button type="button" onclick="redireccionar()" class="btn btn-info btn-info btn-sm">Restablecer búsqueda</button>
                </duv>
        </form>
    </div>
    <table class="table table-hover text-nowrap">
        <thead>
            <tr>
                <th style="width: 100px">ID</th>
                <th style="width: 15px">Tipo Doc.</th>
                <th>N° Documento</th>
                <th>Nombre</th>
                <th>Celular</th>
                <th>Correo</th>
                <th style="width: 200px">Fecha registro</th>
                <th style="width: 100px">Opc</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($estudiantes as $estudiante)
            <tr id="row_{{$estudiante->id}}">
                <td> {{ (string) $estudiante->codigo }} </td>

                <td> {{ $estudiante->tipo_documento }} </td>
                <td> {{ $estudiante->documento_n }} </td>
                <td> {{ $estudiante->nombre }} {{ $estudiante->segundo_nombre }} {{ $estudiante->apellido }}
                    {{ $estudiante->apellido2 }} </td>
                <td> {{$estudiante->celular}} </td>
                <td> {{$estudiante->email}} </td>
                <td> {{  date('d-M-Y',strtotime($estudiante->created_at)) }} </td>
                <td>
                    <a class="open-modal bg-info btn-sm"
                        href="{{route('estudiantes.edit',['estudiante' => $estudiante->id])}}">
                        <i class="fas fa-pencil-alt"></i>
                    </a>

                    <a class="bg-danger btn-sm ml-1" href="javascript:eliminar_registro('{{$estudiante->id}}')">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                </td>
            </tr>
            @endforeach

            @if ($estudiantes->count()==0)
            <tr>
                <td colspan="8" class="text-center"> No hay estudiantes registrados</td>
            </tr>
            @endif

        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    {{ $estudiantes->render("pagination::bootstrap-4") }}
</div>


<!-- /.card-body -->
</div>

<div id="modal-add"></div>


@stop
@section('js')
<script src="js/jquery-ui.min.js"></script>
<script src="/plugins/sweetalert/sweetalert2.min.js"></script>

<script>
$(document).ready(function() {
    $("#buscador").autocomplete({
        source: '<?php echo route("estudiantes.get-sugerencia"); ?>',
        delay: 300,
        autoFocus: false,
        minLength: 1,
        select: function(event, ui) {
            event.preventDefault();
            $("#buscador").val(ui.item.label);
            $("#btn-subtmit").click();

        }
    });

});
function redireccionar(){
    window.location='{{route("estudiantes.index")}}';
}
</script>

<script>

    function eliminar_registro(id){

        data = {
            "id": id,
            "_token": "{{ csrf_token() }}",
        },
        swal({
        title: '',
        text: "Esta seguro de eliminar el estudiante seleccionado?",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
        }).then(function(result) {
            if(result.value){
                $.ajax(
                {
                    url:"estudiantes/"+id,
                    type: 'DELETE',
                    data:data,
                    success :function(reponse){
                        if(reponse.success){
                            toastr.success('Estudiante eliminado exitosamente');
                            $("#row_"+id).remove();

                        }else{
                            toastr.error('No fue posible eliminar el estudiante');
                        }
                    },
                    error: function (response) {
                        toastr.error('Error al procesar la solicitud')
                    }
                });
            }

        });
    }
    
</script>
@endsection