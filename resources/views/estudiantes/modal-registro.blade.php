<link rel="stylesheet" href="css/select2.min.css">

<div class="modal fade" id="modal-lg" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h4 class="modal-title">Datos del estudiante</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="formCursos" autocomplete="off" class="save-ajax" method="{{$object->exists? 'POST':'POST'}}"
                data-has-files='true'
                action="{{ $object->exists? route('estudiantes.update', ['estudiante' => $object->id]):route('estudiantes.store') }}">
                @csrf
                @if($object->exists)
                <input type="hidden" value="{{$object->exists ? $object->id: '0'}}" id="id" name="id">
                <input hidden name="_method" value="PUT">
                @endif
                <div class="modal-body">

                    <div class="card-body row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="tipo_documento">Tipo documento: <span class="text-danger">*</span> </label>
                                <select name="tipo_documento" id="tipo_documento" class="form-control">
                                    <?php  foreach ($tipos_doc as $key => $tipo): ?>
                                    <option value="<?=$key?>" <?= $object->tipo_documento ==$key ? "selected": ""?>>
                                        <?=$tipo?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div data-feedback="tipo_documento" class="text-danger">
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="documento_n">N° Documento: <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="documento_n" name="documento_n"
                                    placeholder="Ingrese N° Documento" value="{{ $object->documento_n }}">
                                <div data-feedback="documento_n" class="text-danger">
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="nombre">Primer Nombre: <span class="text-danger">*</span></label>
                                <input type="text" class="form-control text-capitalize" id="nombre" name="nombre"
                                    placeholder="Ingrese primer nombre" value="{{ $object->nombre }}">
                                <div data-feedback="nombre" class="text-danger">
                                </div>

                            </div>
                        </div>


                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="segundo_nombre">Segundo Nombre: </label>
                                <input type="text" class="form-control text-capitalize" name="segundo_nombre" id="segundo_nombre"
                                    placeholder="Ingrese Segundo nombre" value="{{ $object->segundo_nombre }}">
                                <div data-feedback="segundo_nombre" class="text-danger">
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="apellido">Primer Apellido: <span class="text-danger">*</span></label>
                                <input type="text" class="form-control text-capitalize" name="apellido" id="apellido"
                                    placeholder="Ingrese primer Apellido" value="{{ $object->apellido }}">
                                <div data-feedback="apellido" class="text-danger">
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="apellido2">Segundo Apellido: <span class="text-danger">*</span></label>
                                <input type="text" class="form-control text-capitalize" name="apellido2" id="apellido2"
                                    placeholder="Ingrese segunco Apellido" value="{{ $object->apellido2 }}">
                                <div data-feedback="apellido2" class="text-danger">
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="fecha_expedicion">Fecha de expedición del documento: <span
                                        class="text-danger">*</span></label>
                                <input type="date" class="form-control" name="fecha_expedicion" id="fecha_expedicion"
                                    placeholder="Ingrese fecha" value="{{ $object->fecha_expedicion }}">
                                <div data-feedback="fecha_expedicion" class="text-danger">
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="expedicion_ciudad_id">Lugar de expedición del documento: <span
                                        class="text-danger">*</span></label>

                                <select lass="form-control select2" style="width: 100%;" name="expedicion_ciudad_id" id="expedicion_ciudad_id" >
                                <option value="" >-- Seleccione --</option>    
                                <?php  foreach ($ciudades as $c): ?>
                                    <option value="<?=$c->id?>" <?= $object->expedicion_ciudad_id  == $c->id ? "selected": ""?>>
                                        <?=$c->nombre. " - ".$c->dep_nombre?></option>
                                    <?php endforeach; ?>
                                </select>
                                
                                <div data-feedback="expedicion_ciudad_id" class="text-danger">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="email">Email:<span class="text-danger">*</span> </label>
                                <input class="form-control" name="email" id="email" placeholder="Ingrese Email"
                                    value="{{ $object->email }}">
                                <div data-feedback="email" class="text-danger">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="celular">Celular: </label>
                                <input class="form-control" name="celular" id="celular" placeholder="Ingrese Celular"
                                    value="{{ $object->celular }}">
                                <div data-feedback="celular" class="text-danger">
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.card-body -->

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn-submit btn btn-primary">Guardar</button>
                </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script src="js/select2.full.min.js"></script>
<script>
jQuery(document).ready(function() {

    $("#expedicion_ciudad_id").select2({
        placeholder: "Seleccione una Ciudad",
        theme: 'bootstrap4',
        /*
        id: function(suggestion) {
            return suggestion.id;
        },
        ajax: {
            url: "<?=route("ubicaciones.get-ciudades")?>",
            dataType: 'json',           
            results: function(data, page) {
                return {
                    results: data
                };
            }
        },
        formatSelection: function(suggestion) {
            return suggestion.text;
        },
        formatResult: function(suggestion) {
            return suggestion.text;
        }*/
    });

});
</script>