@extends('adminlte::page')
@section('content_header')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <a href="{{route('sedes.index')}}" class="">
              <!-- small box -->
              <div class="small-box bg-info">
                  <div class="inner p-1">
                    <h3>Sedes</h3>
                    <p>lifecare</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-house-user"></i>
                  </div>
            <a href="#" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
            </div>
            </a>
        </div>
        <!-- ./col -->

          <div class="col-lg-3 col-6">
              <a href="{{route('estudiantes.index')}}" class="">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner p-1">
                      <h3>Estudiantes</h3>
                      <p>lifecare</p>
                    </div>
                    <div class="icon">
                      <i class="fas fa-users"></i>
                    </div>
              <a href="#" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
              </div>
              </a>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
              <a href="{{route('programas.index')}}" class="">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner p-1">
                      <h3>Ofertas</h3>
                      <p>lifecare</p>
                    </div>
                    <div class="icon">
                      <i class="fas fa-book"></i>
                    </div>
              <a href="#" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
              </div>
              </a>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <a href="{{route('cursos.index')}}" class="">
              <!-- small box -->
              <div class="small-box bg-danger">
                  <div class="inner p-1">
                    <h3>Cursos y diplomados</h3>
                    <p>lifecare</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-book"></i>
                  </div>
                  <a href="#" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </a>
          </div>


          <div class="col-lg-3 col-6">
            <a href="{{route('empleados.index')}}" class="">
              <!-- small box -->
              <div class="small-box bg-primary">
                  <div class="inner p-1">
                    <h3>Empleados</h3>
                    <p>lifecare</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-book"></i>
                  </div>
                  <a href="#" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </a>
          </div>

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@stop