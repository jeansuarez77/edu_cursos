@extends('adminlte::page')
@section('title', 'Empleados')

@section('css')
<link rel="stylesheet" href="css/jquery-ui.min.css">
@endsection

@section('content_header')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="/">Inicio</a></li>
                    <li class="breadcrumb-item active">Perfil</li>
                </ol>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@stop


@section('content')


<!-- /.card -->

<div class="card card-outline card-success">
    <div class="card-header">
        <h3 class="card-title font-weight-bold">Gestionar Perfil

    </div>
    <form id="formArea"  class="save-ajax" method="POST" data-has-files='true' action="{{  route('empleados.update', ['empleado' => $object->id])}}">
        @csrf
        @if($object->exists)
            <input hidden name="_method" value="PUT">
        @endif
    <div class="card-body row">
                  
        <div class="col-sm-12">
            <div class="form-group">
                <label for="name">Nombre completo: <span class="text-danger">*</span> </label>
                <input type="text" class="form-control text-capitalize" name="name" id="name" placeholder="Ingrese nombre de usuario" required value="{{ $object->name }}">
                <div data-feedback="name" class="text-danger feedback_error">                           
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="email">Email: <span class="text-danger">*</span> </label>
                <input type="text" class="form-control text-capitalize" name="email" id="email" placeholder="Ingrese Correo electronico" required value="{{ $object->email }}">
                <div data-feedback="email" class="text-danger feedback_error">                           
                </div>
            </div>
        </div>
        <hr class="col-md-12">
        <div class="col-sm-12">
            <div class="form-group">
                <label for="password">Contraseña: <span class="text-danger">*</span> </label>
                <input type="password" class="form-control text-capitalize" name="password" id="password" placeholder="Ingrese contraseña" >
                <div data-feedback="password" class="text-danger feedback_error">                           
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label for="password_confirmation">Confirmar contraseña: <span class="text-danger">*</span> </label>
                <input type="password" class="form-control text-capitalize" name="password_confirmation" id="password_confirmation" placeholder="Confirmar contraseña" >
                
            </div>
        </div>

        <span>* Si la contraseña esta vacia no se actualizara</span>

        

    </div>
    <div class="modal-footer justify-content-between">
        <button type="submit" class="btn btn-submit btn-primary">Actualizar</button>
    </div>
</form>

</div>


<!-- /.card-body -->
</div>

<div id="modal-add"></div>


@stop
@section('js')

@endsection