@extends('adminlte::page')

@section('title', 'Ofertas')



@section('content_header')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="/">Inicio</a></li>
                    <li class="breadcrumb-item active">Ofertas</li>
                </ol>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@stop
@section('content')

    <div class="card card-outline card-success">
        <div class="card-header">
            <h3 class="card-title font-weight-bold">Gestión de Ofertas</h3>

            <div class="card-tools">
                <div class="btn-group">
                    <a  href="{{ route('programas.create') }}" class="btn btn-success open-modal" >
                        <i class="fas fa-plus"></i> Crear Ofertas
                    </a>
                    <a href="{{ route('areas.create') }}" class="btn btn-info  open-modal">
                        <i class="fas fa fa-plus-square"></i> Crear Areas
                    </a>
                </div>

                
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body p-0">
            <table class="table table-hover text-nowrap">
                <thead>
                    <tr>
                        <th style="width: 10px">Codigo</th>
                        <th>Nombre</th>
                        <th>Area</th>
                        <th style="width: 200px">Fecha creación</th>
                        <th style="width: 100px">Opc</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($programas as $programa)
                    <tr id="row_{{$programa->id}}">
                        <td> {{ $programa->codigo }} </td>
                        <td> {{ $programa->nombre }} </td>
                        <td class="font-weight-bold"> {{$programa->area}} </td>
                        <td> {{  date('d-M-Y',strtotime($programa->created_at)) }} </td>
                        <td>
                            <a class="open-modal bg-info btn-sm" href="{{route('programas.edit',['programa' => $programa->id])}}" >
                                <i class="fas fa-pencil-alt"></i>
                            </a>

                            <a class="bg-danger btn-sm ml-1" href="javascript:eliminar_registro('{{$programa->id}}')">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach

                    @if ($programas->count()==0)
                        <tr>
                            <td colspan="5" class="text-center"> No hay programas registrados</td>
                        </tr>
                    @endif

                </tbody>
            </table>
        </div>
        {{ $programas->render() }}

        <!-- /.card-body -->
    </div>

    <div id="modal-add"></div>

@stop
@section('js')
<script src="/plugins/sweetalert/sweetalert2.min.js"></script>
    <script>

        function eliminar_registro(id){
    
            data = {
                "id": id,
                "_token": "{{ csrf_token() }}",
            },
            swal({
            title: '',
            text: "Esto podria afectar los cursos abiertos, esta seguro de eliminar la oferta seleccionada y cursos abiertos?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
            }).then(function(result) {
                if(result.value){
                    $.ajax(
                    {
                        url:"programas/"+id,
                        type: 'DELETE',
                        data:data,
                        success :function(reponse){
                            if(reponse.success){
                                toastr.success('Oferta eliminada exitosamente');
                                $("#row_"+id).remove();
    
                            }else{
                                toastr.error('No fue posible eliminar la oferta');
                            }
                        },
                        error: function (response) {
                            toastr.error('Error al procesar la solicitud')
                        }
                    });
                }
    
            });
        }
        
    </script>

@stop
