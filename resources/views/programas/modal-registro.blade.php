<div class="modal fade" id="modal-lg" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-success">
          <h4 class="modal-title">Gestión de Ofertas</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form id="formProgramas"  class="save-ajax" method="{{$object->exists? 'POST':'POST'}}" data-has-files='true' action="{{ $object->exists? route('programas.update', ['programa' => $object->id]):route('programas.store') }}">
            @csrf
            @if($object->exists)
                <input type="hidden" value="{{$object->exists ? $object->id: '0'}}"  id="id" name="id">
                <input hidden name="_method" value="PUT">
            @endif
            <div class="modal-body">
                
                <div class="card-body row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="codigo">Codigo del programa: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="codigo" name="codigo" placeholder="Ingrese codigo del programa"  value="{{ $object->codigo }}">
                            <div data-feedback="codigo" class="text-danger">                           
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Area: <span class="text-danger">*</span> </label>
                            <select name="area_id" class="form-control">
                            @foreach ($areas as $area)

                            @if ($area->id == $object->area_id)

                                <option value="{{$area->id}}" selected>{{$area->nombre}}</option>
                            @else
                                <option value="{{$area->id}}">{{$area->nombre}}</option>
                            @endif

                            @endforeach
                            </select>
                            <div data-feedback="area_id" class="text-danger">                           
                            </div>

                        </div>
                    </div>
                    
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="nombre">Nombre del Programa: <span class="text-danger">*</span> </label>
                            <input type="text" class="form-control text-capitalize" name="nombre" id="nombre" placeholder="Ingrese nombre del programa" value="{{ $object->nombre }}">
                            <div data-feedback="nombre" class="text-danger">                           
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Descripción: <span class="text-danger">*</span> </label>
                            <textarea class="form-control" rows="7" name="descripcion" placeholder="Ingrese descripción del programa"> {{$object->descripcion}} </textarea>
                            <div data-feedback="descripcion" class="text-danger">                           
                            </div>

                        </div>
                    </div>

                </div>
                    <!-- /.card-body -->

            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </form>

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>