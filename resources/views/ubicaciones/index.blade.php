@extends('adminlte::page')

@section('css')
<link rel="stylesheet" href="css/jquery-ui.min.css">
@endsection

@section('content_header')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="/">Inicio</a></li>
                    <li class="breadcrumb-item active">Sedes</li>
                </ol>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@stop


@section('content')


<!-- /.card -->

<div class="card card-outline card-success">
    <div class="card-header">
        <h3 class="card-title font-weight-bold">Gestión de sedes <span class="badge badge-success">
                {{  $ubicaciones->total()}}</span></h3>

        <div class="card-tools">
            <div class="btn-group">
                <a href="{{ route('sedes.create') }}" class="btn btn-success open-modal">
                    <i class="fas fa-plus"></i> Crear sede
                </a>
            </div>


        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0">
        <form action="{{ route('sedes.index') }}" method="get">
            <div class="row p-1">
                <duv class="col-md-3">
                    <div class="input-group input-group-sm">
                        <input id="buscador" name="buscar" type="text" value="{{$busqueda}}"
                            placeholder="Ingresa un palabra para Buscar" class="form-control">
                        <span class="input-group-append">
                            <button type="submit" id="btn-subtmit" class="btn btn-info btn-flat">Buscar</button>
                        </span>
                    </div>
                </duv>
                <duv class="col-md-3">
                    <button type="button" onclick="redireccionar()" id="btn-subtmit" class="btn btn-info btn-info btn-sm">Restablecer búsqueda</button>
                </duv>
        </form>
    </div>
    <table class="table table-hover text-nowrap">
        <thead>
            <tr>
                <th style="width: 10px">ID</th>
                <th>Nombre</th>
                <th>Prefijo</th>
                <th>Ciudad</th>
                <th>Dirección</th>
                <th>Fecha registro</th>
                <th style="width: 100px">Opc</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($ubicaciones as $ubicacion)
            <tr>
                <td> {{ $ubicacion->id }} </td>

                <td> {{ $ubicacion->nombre }} </td>
                <td> {{ $ubicacion->prefijo }} </td>
                <td> {{$ubicacion->ciudad}} </td>
                <td> {{$ubicacion->direccion}} </td>
                <td> {{  date('d-M-Y',strtotime($ubicacion->created_at)) }} </td>
                <td>
                    <a class="open-modal bg-info btn-sm"
                        href="{{route('sedes.edit',['sede' => $ubicacion->id])}}">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                </td>
            </tr>
            @endforeach

            @if ($ubicaciones->count()==0)
            <tr>
                <td colspan="5" class="text-center"> No hay sedes registradas</td>
            </tr>
            @endif

        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    {{ $ubicaciones->render("pagination::bootstrap-4") }}
</div>


<!-- /.card-body -->
</div>

<div id="modal-add"></div>


@stop
@section('js')
<script src="js/jquery-ui.min.js"></script>
<script>
$(document).ready(function() {
    $("#buscador").autocomplete({
        source: '<?php echo route("sedes.get-sugerencia"); ?>',
        delay: 300,
        autoFocus: false,
        minLength: 1,
        select: function(event, ui) {
            event.preventDefault();
            $("#buscador").val(ui.item.label);
            $("#btn-subtmit").click();

        }
    });

});
function redireccionar(){
    window.location='{{route("sedes.index")}}';
}
</script>
@endsection