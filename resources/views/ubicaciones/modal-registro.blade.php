<link rel="stylesheet" href="css/select2.min.css">

<div class="modal fade" id="modal-lg" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h4 class="modal-title">Datos de la sede</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="formCursos" autocomplete="off" class="save-ajax" method="{{$object->exists? 'POST':'POST'}}"
                data-has-files='true'
                action="{{ $object->exists? route('sedes.update', ['sede' => $object->id]):route('sedes.store') }}">
                @csrf
                @if($object->exists)
                <input type="hidden" value="{{$object->exists ? $object->id: '0'}}" id="id" name="id">
                <input hidden name="_method" value="PUT">
                @endif
                <div class="modal-body">

                    <div class="card-body row">
                        
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="nombre">Nombre: <span class="text-danger">*</span></label>
                                <input type="text" class="form-control text-capitalize" id="nombre" name="nombre"
                                    placeholder="Ingrese nombre de la sede" value="{{ $object->nombre }}">
                                <div data-feedback="nombre" class="text-danger">
                                </div>

                            </div>
                        </div>


                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="ciudad_id">Ciudad: <span
                                        class="text-danger">*</span></label>

                                <select lass="form-control select2" style="width: 100%;" name="ciudad_id" id="ciudad_id" >
                                <option value="" >-- Seleccione --</option>    
                                <?php  foreach ($ciudades as $c): ?>
                                    <option value="<?=$c->id?>" <?= $object->ciudad_id  == $c->id ? "selected": ""?>>
                                        <?=$c->nombre. " - ".$c->dep_nombre?></option>
                                    <?php endforeach; ?>
                                </select>
                                
                                <div data-feedback="ciudad_id" class="text-danger">
                                </div>
                            </div>
                        </div>
                        

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="direccion">Dirección: <span class="text-danger">*</span></label>
                                <input type="text" class="form-control text-uppercase" name="direccion" id="direccion"
                                    placeholder="Ingrese direccion" value="{{ $object->direccion }}">
                                <div data-feedback="direccion" class="text-danger">
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="prefijo">Prefijo: <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="prefijo" id="prefijo"
                                    placeholder="Ingrese prefijo identificador" value="{{ $object->prefijo }}">
                                <div data-feedback="prefijo" class="text-danger">
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="rango_inicio">Rango de inicio: <span class="text-danger">*</span></label>
                                <input type="number" class="form-control text-capitalize" name="rango_inicio" id="rango_inicio"
                                    placeholder="Ingrese rango de inicio consecutivo" value="{{ $object->rango_inicio }}">
                                <div data-feedback="rango_inicio" class="text-danger">
                                </div>

                            </div>
                        </div>


                    </div>
                    <!-- /.card-body -->

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn-submit btn btn-primary">Guardar</button>
                </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script src="js/select2.full.min.js"></script>
<script>
jQuery(document).ready(function() {

    $("#ciudad_id").select2({
        placeholder: "Seleccione una Ciudad",
        theme: 'bootstrap4',
        
    });

});
</script>