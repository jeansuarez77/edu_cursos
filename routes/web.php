<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EstudiantesController;
use App\Http\Controllers\ProgramasController;
use App\Http\Controllers\AreasController;
use App\Http\Controllers\CertificadosController;
use App\Http\Controllers\CursosController;
use App\Http\Controllers\UbicacionesController;
use App\Http\Controllers\GeneradorController;
use App\Http\Controllers\EmpleadosController;
use App\Http\Controllers\AulasController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    
    Route::get('/', function () {
        return view('home');
    });

    Route::resource('estudiantes', EstudiantesController::class);    
    Route::group(['prefix' => 'gestion-estudiante'], function () {   
        Route::get('get_sugerencia',  [EstudiantesController::class ,'get_sugerencia'])->name('estudiantes.get-sugerencia');
       
    });
    Route::resource('programas', ProgramasController::class);
    Route::resource('areas', AreasController::class);

    Route::group(['prefix' => 'ubicaciones'], function () {   
        Route::get('get_ciudades',  [UbicacionesController::class ,'get_ciudades'])->name('ubicaciones.get-ciudades');
       
    });
    
    Route::resource('sedes', UbicacionesController::class);
    Route::group(['prefix' => 'gestion-sedes'], function () {   
        Route::get('get_sugerencia',  [UbicacionesController::class ,'get_sugerencia'])->name('sedes.get-sugerencia');
       
    });
    
    Route::resource('cursos', CursosController::class);
    Route::group(['prefix' => 'gestion-cursos'], function () {   
        Route::get('edit_estudiantes/{curso}',  [CursosController::class ,'edit_estudiantes'])->name('cursos.get-estudiantes');
        Route::post('agregar_estidiantes',  [CursosController::class ,'agregar_estidiantes'])->name('cursos.add-estudiantes');
        Route::post('aprobar_estidiantes',  [CursosController::class ,'aprobar_estidiantes'])->name('cursos.aprobar-estudiantes');
        Route::get('get_sugerencia',  [CursosController::class ,'get_sugerencia'])->name('cursos.get-sugerencia');
       
    });

    Route::resource('aulas', AulasController::class);

    Route::get('empleados/profile',  [EmpleadosController::class ,'profile'])->name('empleados.profile');

    Route::resource('empleados', EmpleadosController::class);
    Route::resource('certificados', CertificadosController::class);
   // Auth::routes(["register" => false]);
});
Route::group(['prefix' => 'certificados-digitales'], function () {   
    Route::get('get_certificado_curso',  [GeneradorController::class ,'get_certificado_curso']);
    Route::get('',  [GeneradorController::class ,'index']);
    Route::get('get_lita_certificados',  [GeneradorController::class ,'get_lita_certificados']);   
   
});

/*
    //Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    //Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    //Route::post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
 */

/*Route::get('/register',function(){
    return abort(404);
 })->name('register')->middleware('guest');*/


